package main

import (
	"encoding/csv"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
)

type UserForWrite struct {
	Email     string `json:"email"`
	CreatedAt string `json:"created_at"`
}

type UserForRead struct {
	Email       string
	CreatedAt   string `json:"Created_at"`
	Subscribers []UserForRead
}

type User struct {
	Email string
	Subs  map[string]*User
}

type Path struct {
	ID   float64        `json:"id"`
	From string         `json:"from"`
	To   string         `json:"to"`
	Path []UserForWrite `json:"path,omitempty"`
}

func main() {
	usersForRead, err := readUsers("users.json")
	if err != nil {
		log.Fatalf("can't read users: %s", err)
	}

	users, err := getUsers(usersForRead)
	if err != nil {
		log.Fatalf("can't get users: %s", err)
	}

	input, err := getInput("input.csv")
	if err != nil {
		log.Fatalf("can't get input: %s", err)
	}

	paths, err := findallPaths(users, input, usersForRead)
	if err != nil {
		log.Fatalf("can't find all paths: %s", err)
	}

	err = writeJSON(paths, "result.json")
	if err != nil {
		log.Fatalf("can't writeJson: %s", err)
	}
}

func writeJSON(paths []Path, name string) error {
	data, err := json.MarshalIndent(paths, "", "\t")
	if err != nil {
		return fmt.Errorf("can't parse usersForWrite in json: %s", err)
	}

	err = ioutil.WriteFile(name, data, 0777)
	if err != nil {
		return fmt.Errorf("can't write data in %s : %s", name, err)
	}

	return nil
}

func userInPath(email string, path []string) bool {
	for _, user := range path {
		if email == user {
			return true
		}
	}

	return false
}

func rec(user *User, path []string, to string) []string {
	var innerPath []string

	var rePath []string

	for email := range user.Subs {
		if email == to {
			return path
		}
	}

	for email, sub := range user.Subs {
		if userInPath(email, path) {
			continue
		}

		innerPath = rec(sub, append(path, email), to)
		if (innerPath != nil && len(innerPath) < len(rePath)) || rePath == nil {
			rePath = innerPath
		}
	}

	return rePath
}

func findPath(users map[string]*User, from string, to string, usersForRead []UserForRead) ([]UserForWrite, error) {
	var path []string

	var usersWrite []UserForWrite

	var flag bool

	if from == to {
		return nil, nil
	}

	path = rec(users[from], nil, to)
	if path == nil {
		return nil, nil
	}

	for _, email := range path {
		flag = false

		for _, user := range usersForRead {
			if user.Email == email {
				userWrite := UserForWrite{email, user.CreatedAt}
				usersWrite = append(usersWrite, userWrite)
				flag = true

				break
			}
		}

		if !flag {
			return nil, fmt.Errorf("can't find user by email %s", email)
		}
	}

	return usersWrite, nil
}

func findallPaths(users map[string]*User, input [][]string, usersForRead []UserForRead) ([]Path, error) {
	const (
		from  = 0
		to    = 1
		shift = 1
	)

	var result []Path

	for i, fromTo := range input {
		currResult := Path{ID: (float64)(i + shift), From: fromTo[from], To: fromTo[to]}
		innPath, err := findPath(users, fromTo[from], fromTo[to], usersForRead)

		if err != nil {
			return nil, fmt.Errorf("can't dind path %s", err)
		}

		currResult.Path = innPath
		result = append(result, currResult)
	}

	return result, nil
}

func getInput(name string) (input [][]string, err error) {
	const (
		numberOfParams = 2
	)

	file, err := os.Open(name)
	if err != nil {
		return nil, fmt.Errorf("can't open file %s: %s", name, err)
	}

	defer file.Close()

	reader := csv.NewReader(file)

	for {
		record, err := reader.Read()
		if err == io.EOF {
			break
		}

		if err != nil {
			return nil, fmt.Errorf("can't read file %s: %s", name, err)
		}

		if len(record) != numberOfParams {
			return nil, fmt.Errorf("wrong number fo params in file %s", name)
		}

		input = append(input, record)
	}

	return input, nil
}

func readUsers(name string) ([]UserForRead, error) {
	var usersForRead []UserForRead

	file, err := ioutil.ReadFile(name)
	if err != nil {
		return nil, fmt.Errorf("can't open file %s: %s", name, err)
	}

	err = json.Unmarshal(file, &usersForRead)
	if err != nil {
		return nil, fmt.Errorf("can't parse file %s: %s", name, err)
	}

	return usersForRead, nil
}

func getUsers(usersForRead []UserForRead) (map[string]*User, error) {
	var ok bool

	users := make(map[string]*User)

	for _, userRead := range usersForRead {
		user := User{userRead.Email, nil}
		users[userRead.Email] = &(user)
	}

	for _, userRead := range usersForRead {
		subscriptions := make(map[string]*User)

		for _, userInner := range usersForRead {
			for _, subscriber := range userInner.Subscribers {
				if subscriber.Email == userRead.Email {
					subscriptions[userInner.Email], ok = users[userInner.Email]

					if !ok {
						return nil, fmt.Errorf("did't find user by email : %s", subscriber.Email)
					}
				}
			}
		}

		puser := users[userRead.Email]
		puser.Subs = subscriptions
	}

	return users, nil
}
